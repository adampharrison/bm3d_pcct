**Non-Proprietary Software Transfer Agreement**
Provider: National Institutes of Health (NIH), Clinical Center (CC)

This project utilizes code from [CBM3D](http://www.cs.tut.fi/~foi/GCF-BM3D/index.html#ref_software). Therefore, all code and content created by the authors of CBM3D is [licensed under the TUT Limited License](http://www.cs.tut.fi/~foi/GCF-BM3D/legal_notice.html). All contributions and code added to this project are [dedicated to the public domain worldwide](https://creativecommons.org/publicdomain/zero/1.0/). 

## Public Domain

This project constitutes a work of the United States Government and is not subject to domestic copyright protection under 17 USC § 105. Additionally, we waive copyright and related rights in the work worldwide through the [CC0 1.0 Universal public domain dedication](https://creativecommons.org/publicdomain/zero/1.0/).

All contributions to this project will be released under the CC0 dedication. By submitting a pull request, you are agreeing to comply with this waiver of copyright interest. See [CONTRIBUTING](https://github.com/GSA/data.gov/blob/master/CONTRIBUTING.md) for more information. 

## Other Information

In no way are the patent or trademark rights of any person affected by CC0, nor are the rights that other persons may have in the work or in how the work is used, such as publicity or privacy rights.

The Software transferred under this agreement was created by Federal Government employees in the course of official duties. Transfer of Software to Recipient does not constitute endorsement by the NIH CC of the Recipient or any product, service or company and no endorsement should be inferred. SOFTWARE IS NOT INTENDED FOR TREATING OR DIAGNOSING HUMAN SUBJECTS.

BM3D\_PCCT may be updated periodically as new data or research becomes available.

Software is supplied AS IS, without any accompanying services or improvements from NIH. SOFTWARE IS SUPPLIED TO RECIPIENT WITH NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING ANY WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. NIH CC makes no representations that the use of Software will not infringe any patent or proprietary rights of third parties.
All risk as to quality and performance of Software is with Recipient. In no event will the United States Government or NIH CC be liable to Recipient for damages arising out of the use or inability to use Software, including but not limited to loss of data or data being rendered inaccurate or losses sustained by Recipient or third parties
