function ims = double_to_hounsfields( ims,old_ranges )
%given images in range [0,1], rescales them to range given by old_ranges
bins=size(ims,3);

for i=1:bins
    
    
    ims(:,:,i)=ims(:,:,i)*(old_ranges(i,2)-old_ranges(i,1))+old_ranges(i,1);

end


end

