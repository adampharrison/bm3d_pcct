function [ims, old_ranges]=hounsfields_to_double(ims,shared_range,ranges)
%given images in large range, scales them to [0,1].,If shared_range is
%given, all image channels will be rescaled using the same range, that is
%the min and max value of all channels, otherwise it will use
%channel-specific min and max range. If ranges is given, the function will
%just use the given ranges. 
if(~exist('ranges'))
    
    bins=size(ims,3);
    old_ranges=zeros(bins,2);
    if(~exist('shared_range'))
        shared_range=false;
    end
    if shared_range
        
            
       old_ranges(:,1)=ones(bins,1)*min(ims(:));
       old_ranges(:,2)=ones(bins,1)*max(ims(:));
       ims=(ims-min(ims(:)))/(max(ims(:)-min(ims(:))));

        
    else
        for i=1:bins
            bin=ims(:,:,i);
            old_ranges(i,1)=min(bin(:));
            old_ranges(i,2)=max(bin(:));
            ims(:,:,i)=(bin-old_ranges(i,1))/(old_ranges(i,2)-old_ranges(i,1));

        end
    end
else
    bins=size(ims,3);    
    for i=1:bins
        bin=ims(:,:,i);        
        ims(:,:,i)=(bin-ranges(i,1))/(ranges(i,2)-ranges(i,1));

    end
end


