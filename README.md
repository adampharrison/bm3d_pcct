BM3D\_PCCT is a a modification of the [CBM3D](http://www.cs.tut.fi/~foi/GCF-BM3D/index.html#ref_software) algorithm for multi-channel PCCT data. It can likely be applied to any other appropriate multi-channel data. It adapts CBM3D in two ways: (1) it uses a guide image, which should have higher SNR, to compute a shared grouping that all channels use. For PCCT data, this is the all-photon image. (2) it decorrelates image channels beforehand with a DCT transform to gain a sparse representation of spectral coefficients. These two adaptations allow it to acheive greater performance for denoising PCCT imagery. 

## Usage

Install the [BM3D Software Suite](http://www.cs.tut.fi/~foi/GCF-BM3D/index.html#ref_software). Once you have done that, and added the appropriate paths to the BM3D mex files to your MATLAB environment, you should be able to run BM3D\_PCCT.

### Citations

If you are using the code/model/data provided here in a publication, please cite our paper:

    @Article{harrison_adam_2017,
      author = {"Adam P. Harrison and Ziyue Xu and David A. Bluemke and Amir Pourmorteza and Daniel J. Mollura"},
      Title = {A multi-channel block-matcing denoising algorithm for spectral photon counting CT},
      Journal = "Medical Physics",
      Year  = {Accepted},
    }

